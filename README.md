# Nginx Ingress Controller

In CNO, the [Nginx Ingress controller](https://kubernetes.github.io/ingress-nginx/) is used to reverse-proxy and SSL-terminate incoming traffic to the Kubernetes cluster.

__Platform Administrators__ use the Ingress Controller to disclose ingress traffic for CNO-components (GitLab, Harbor, ...).

__Application Administrators__ use the Ingress Controller to disclose ingress traffic for their applications.

# Contents

This project defines an Ansible role for deploying the Nginx Ingress Controller.

Prerequisites:

- [Certmanager](https://gitlab.com/logius/cloud-native-overheid/components/certmanager)
- [Prometheus CRDs](https://gitlab.com/logius/cloud-native-overheid/components/prometheus#install-crds)

In this project:

- Local installation, test and development (all optional)
- Datacenter installation
- Design notes

Local installation assumes the [platform-runner](https://gitlab.com/logius/cloud-native-overheid/components/platform-runner) to run in a Docker image.

# Install options

| Option      | Default           | Description  |
| --- |:---:| ---|
| `config.nginx_ingress.use_deamonset`  | `true` | Deploy nginx as a daemonset on all (selected) nodes |
| `config.nginx_ingress.use_host_port`  | `true` | enable `hostport` for `service_type` `ClusterIP` or `Daemonset`  |
| `config.nginx_ingress.use_node_port`  | `false` | Deploy service with allocated nodeport |
| `config.nginx_ingress.service_type` | `LoadBalancer` |  Define a service type for nginx. One of `ClusterIP`, `LoadBalancer`, `NodePort` |
| `config.nginx_ingress.service_annotations`     | `{}`     |   Add annotations for a service for assigning specific service resources |
| `config.nginx_ingress.node_selector_label` | `{"node-role":"nginx-ingress-controller"}` | set a configured tag on the listed nodes and use that to deploy to the correct nodes |
| `config.nginx_ingress.nodes` | `[]` | configure the nodes where nginx pods are running. Useful when using HostPort and you don't want to run nginx on all nodes. |
| `config.nginx_ingress.custom_node_selector` | `{}` | use an existing tag to deploy to the correct nodes |
| `config.nginx_ingress.use-proxy-protocol` | `false` | enable proxy protocol for forwarding client request headers |
| `config.nginx_ingress.enable-modsecurity` | `false` | enable web application firewall |
| `config.nginx_ingress.enable-owasp-modsecurity-crs` | `false` | enable the Open Web Application Security Project Core Rule Set |
| `config.nginx_ingress.extra_args` | `{}` | extra startup parameters for the nginx ingress controller |
| `config.nginx_ingress.enable-ssl-passthrough` | `false` | ssl termination on server side, instead of the ingress controller itself |
| `config.nginx_ingress.ingressClassResource.name` | `nginx` |  provide a alternative ingress class name |
| `config.nginx_ingress.ingressClassResource.default` | `false` |  defines if this is the default ingress-controller and will pick up untagged ingressess |
| `config.nginx_ingress.ingressClassResource.controllerClass` | `"k8s.io/ingress-nginx"` |  provide a alternative controller class name |
| `config.nginx_ingress.use_gzip` | `false` |  When true enbles streaming gzip compression ( HTTP-HEADER -> "content-encoding: gzip" ) |

# Local installation

Get platform runner:
```
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

Connect to Kubernetes cluster, e.g.:
```
export KUBECONFIG=$HOME/.kube/config
```

The ingress controller will be installed as a daemon set at the nodes in your kubernetes cluster:
```
kubectl get nodes
```

In working directory, add or extend `cluster_config.yaml`, e.g.:
```
platform:
  prefix: "local"
  domain: example.com # Use your own domain, ref. certmanager

config:
  certmanager:
    domain:  example.com
    issuer: example-issuer # Use your own domain prefix instead of "example"

  nginx_ingress:
    nodes:
      - kind-worker # Use your own worker node(s) based on `kubectl get nodes`
      - kind-worker2
      - kind-worker3
```

From working directory, install Nginx Ingress Controller:
```
# Prepare Ansible command, assuming above cluster_config.yaml will be mounted below
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/nginx-ingress \
  -vv"

# Run Ansible role, using ANSIBLE_CMD with mounted cluster config
docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

The installation uses dependencies as defined in `./meta/main.yml`.

The reading order for Ansible parameterization is: `defaults/main.yml`, overlayed with `$PWD/cluster_config.yaml`.

# Local test

Smoke test, unit test, integration test:

## Smoke test

Check deployment:
```
# Ingress Controller is deployed as daemon set on each labeled node
kubectl get nodes --show-labels | grep standaardplatform
kubectl get daemonsets -n local-nginx-ingress
kubectl get pods -n local-nginx-ingress

# Ingress Controller deployment requested a wildcard certificate via the CertManager
kubectl get CertificateRequest -n local-nginx-ingress -o yaml
kubectl get Order -n local-nginx-ingress -o yaml
kubectl get Challenges -n local-nginx-ingress -o yaml
kubectl get Certificate -n local-nginx-ingress -o yaml # May take some time (5 mins.) to complete

# The corresponding secret
kubectl get secret local-wildcard-certificate -n local-nginx-ingress -o jsonpath='{.data.tls\.crt}' | base64 --decode
```

Test Nginx response on IP-address, to conclude that the Ingress Controller is active:
```
export ENV_WORKER_IP=$(kubectl get nodes -o wide | grep "worker .*Ready" | awk '{print $6}'); echo ${ENV_WORKER_IP}
curl -v ${ENV_WORKER_IP} # Observe Nginx reponse
```

## Unit test

Yet unavailable.

## Integration test

Scope:

- Webapp with Ingress at wildcard cert
- Webapp with Ingress at custom cert

### Webapp with Ingress at wildcard cert

Create namespace:
```
kubectl create namespace local-app
```

Adjust the example domain in a deployment manifest copy:
```
mkdir -p tmp
cp examples/app.yaml tmp # Edit the copy
```

Deploy webapp:
```
kubectl apply -f tmp/app.yaml -n local-app
```

Smoke test webapp:
```
kubectl get pods -n local-app
```

Add the Ingress hostname to your `/etc/hosts`, using the earlier (above set) environment variables; for example on Linux:
```
export ENV_WORKER_IP=$(kubectl get nodes -o wide | grep "worker .*Ready" | awk '{print $6}'); echo ${ENV_WORKER_IP}
sudo echo "${ENV_WORKER_IP} app.example.com" | sudo tee -a /etc/hosts # Use your own domain instead of "example.com"
```

Test webapp:
```
curl https://app.example.com | grep Hello # Use your own domain instead of "example.com"
```

Also test that the wildcard certificate from the Ingress Controller is used:
```
# Observe wildcard certificate is matched under Server certificate
curl -v https://app.example.com # Idem
```

### Webapp with Ingress at custom cert

Adjust the example domain in a deployment manifest copy:
```
mkdir -p tmp
cp examples/cert.yaml tmp # Edit the copy
```

Deploy certificate definition:
```
kubectl apply -f tmp/cert.yaml -n local-app
```

Smoke test custom certificate:
```
# Observe "dnsNames", may take some minutes to complete
kubectl get Certificate -n local-app -o yaml
kubectl get secret app-${ENV_DOMAIN_PREFIX}-tls -n local-app -o jsonpath='{.data.tls\.crt}' | base64 --decode
```

Activate custom certificate in webapp:
```
# In tmp/app.yaml, uncomment and edit "secretName: app-example-tls"
```

Redeploy the webapp:
```
kubectl apply -f tmp/app.yaml -n local-app
```

In the browser, explore the certificate again and observe a custom certificate.

Cleanup:
```
kubectl delete namespace local-app
```

Troubleshooting:
```
kubectl exec -it <one of the Nginx ingress controller pods> --bash
cat /etc/nginx/nginx.conf | grep <your domain>
```

# Local development

Checkout this project, e.g.:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/nginx-ingress.git
```

Checkout projects for dependent roles as defined in `$PWD/nginx-ingress/meta/main.yml`:
```
# At present none
```

Develop and install, this time with `ANSIBLE_ROLES_PATH` and `galaxy_url` referring to local directory:
```
export ANSIBLE_ROLES_PATH=$PWD

ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=nginx-ingress \
  -vv"

docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -v $ANSIBLE_ROLES_PATH:/playbook/roles \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

Optional cleanup using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=nginx-ingress \
  -t remove \
  -vv"
```

__Attention__: avoid the cleanup when you are about to install other CNO components subsequently.

# Datacenter installation

In your own CI/CD, create a pipeline similar to the local installation.

Use a specific tag for `platform-runner`.

# Design notes

Yet unavailable.

# Ref

## Ingress Controller

https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/

## Ingress Controller - Nginx

https://www.nginx.com/products/nginx-ingress-controller/

https://kubernetes.github.io/ingress-nginx/deploy/

https://github.com/nginxinc/kubernetes-ingress/blob/master/deployments/helm-chart/templates/controller-daemonset.yaml

https://stackoverflow.com/questions/58001524/expose-ngnix-ingress-controller-as-daemon-set

## Ingress Controller - Nginx - multiple

https://kubernetes.github.io/ingress-nginx/user-guide/multiple-ingress/

https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/multiple-ingress.md#multiple-ingress-controllers
